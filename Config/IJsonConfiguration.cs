﻿namespace DOG.RepoNook.Config
{
    public interface IJsonConfiguration
    {
        string AtlasMongoConnection { get; }
    }
}